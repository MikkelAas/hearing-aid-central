import java.util.HashMap;
import java.util.Iterator;

/**
 * This class generates objects that are meant to simulate different instances of hearing aid centrals
 * @author 10161
 * @version 12.12.2019
 */
public class HearingAidCentral {
    /**
     * This field stores a HashMap with keys of type String, and values as HearingAid
     */
    private HashMap<String, HearingAid> hearingAidCentral;
    /**
     * This field stores the name of the central as type String
     */
    private String centralName;

    /**
     * This constructor generates instances of the class hearing aid central
     * @param centralName centralName takes a String value and assigns it to the name field
     */
    public HearingAidCentral(String centralName){
        hearingAidCentral = new HashMap<>();
        this.centralName = centralName;
    }

    /**
     * This method accesses the name field of an instance
     * @return Return the name of the central as type String
     */
    public String getCentralName(){
        return centralName;
    }

    /**
     * Adds test instances to the central
     */
    public void addTestHearingAids(){
        for (int i = 0; i < 10; i++){
            hearingAidCentral.put("100" + i, new HearingAid("100" + i, "type" + i, false, null));
        }
    }

    /**
     * This method adds a new hearing aid to the central
     * The rent-status and the name of the owner have default values
     * When we create a new hearing aid it wont be rented out instantly and it will therefore not have an owner
     * upon its creation
     * @param uniqueID uniqueID takes a String value and checks if there are other instances with the same value and
     *                 sets the unique ID of the new hearing aid
     * @param type type takes a String value and sets the type of the hearing aid
     */
    public void addNewHearingAid(String uniqueID, String type){
        Iterator<HearingAid> hearingAidIterator = hearingAidCentral.values().iterator();
        boolean done = false;
        boolean exists = false;
        while(hearingAidIterator.hasNext() && !done){
            HearingAid hearingAid = hearingAidIterator.next();
            if (hearingAid.getUniqueID().equals(uniqueID)){

                done = true;
                exists = true;
            } else {
                exists = false;
            }

        }
        if (exists) {
            System.out.println("Dette hjelpemidlet eksisterer allerede.");
        } else {
            System.out.println("Hjelpemidlet ble lagt til!");
            hearingAidCentral.put(uniqueID, new HearingAid(uniqueID, type, false, null));
        }

    }

    /**
     * Registers a new rental status for an already existing instance of HearingAid
     * @param uniqueID uniqueID takes a String value and checks if the hearing aid we want to rent out exists
     * @param nameOfOwner nameOfOwner takes a String value and sets it as the new current owner
     * @param rentedStatus rentedStatus takes a boolean value and checks whether or not if the hearing aid we want to
     *                     rent out is already rented, and if that's not the case, sets it as the new rental status
     */
    public void registerRental(String uniqueID, String nameOfOwner, boolean rentedStatus){
        boolean found = false;
        for (HearingAid hearingAid : hearingAidCentral.values()){
            if (hearingAid.getUniqueID().equals(uniqueID) && hearingAid.isRentalStatus()){
                System.out.println("Dette hjelpemiddelet er allerede utlånt");
                found = true;
            } else if (hearingAid.getUniqueID().equals(uniqueID) && !hearingAid.isRentalStatus()){
                System.out.println("Endret statusen på hjelpemiddelet.");
                hearingAid.setNameOfOwner(nameOfOwner);
                hearingAid.setRentalStatus(rentedStatus);
                found = true;
            } else {
                found = false;
            }

        }
        if (!found){
            System.out.println("Fant ikke hjelpemiddelet med denne ID'en");
        }
    }

    /**
     * Ends the rental of a hearing aid
     * @param uniqueID uniqueID takes a String value and is used to check for a hearing aid with that ID
     */
    public void endRental(String uniqueID){
        boolean found = false;
        for (HearingAid hearingAid : hearingAidCentral.values()){
            if (hearingAid.getUniqueID().equals(uniqueID) && hearingAid.isRentalStatus()){
                hearingAid.setRentalStatus(false);
                hearingAid.setNameOfOwner(null);
                found = true;
            } else if (hearingAid.getUniqueID().equals(uniqueID) && !hearingAid.isRentalStatus()){
                System.out.println("Dette hjelpemiddelet er ikke utlånt");
                found = true;
            }

        }
        if (!found) {
            System.out.println("Fant ikke hjelpemiddelet med denne ID'en");
        }
    }

    /**
     * Lists the info of all the values (hearing aid) in the central
     */
    public void listAll(){
        for (HearingAid hearingAid: hearingAidCentral.values()){
            hearingAid.printInfo();
        }
    }

    /**
     * Lists only the info of a certain type of instances
     * @param type
     */
    public void listAllOfType(String type){
        for (HearingAid hearingAid : hearingAidCentral.values()){
            if (hearingAid.getType().equals(type)){
                hearingAid.printInfo();
            }
        }
    }
}

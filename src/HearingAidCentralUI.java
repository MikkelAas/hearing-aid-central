import java.util.Iterator;

/**
 * This class generates objects that simulates hearing aid central user interfaces
 * @author 10161
 * @version 12.12.2019
 */
public class HearingAidCentralUI {
    /**
     * This field stores a central of type HearingAidCentral
     */
    private HearingAidCentral central;
    /**
     * This field stores input of type UserInput
     */
    private UserInput input;

    /**
     * This constructor constructs a new instance of the class HearingAidCentralUI
     */
    public HearingAidCentralUI(){
        central = new HearingAidCentral("NTNU-sentralen");
        input = new UserInput();
    }

    public void addTests(){
        central.addTestHearingAids();
    }


    /**
     * This method prints a list of Strings that gives the user information about the available commands
     */
    public void printHelp(){
        System.out.println("Her er de tilgjengelige kommandoene: ");
        System.out.println("LEGG TIL: Legger til et nytt hjelpemiddel.");
        System.out.println("PRINT: Printer alle hjelpemidlene og deres utleie-status.");
        System.out.println("REGISTRER NY: Registrer nytt utleie av et hjelpemiddel.");
        System.out.println("FJERN UTLEIE: Fjerner utleie-statusen til et hjelpemiddel.");
        System.out.println("AVSLUTT: Avslutter programmet.");
    }

    /**
     * This method lets the user add a new instance of HearingAid
     */
    public void addNewHearingAid(){
        boolean done = false;
        while (!done){
            System.out.println("Vennligst skriv inn den unike ID'en:");
            String uniqueID = input.getStringInputBetween();
            System.out.println("Vennligst skriv inn typen hjelpemiddel:");
            String type = input.getStringInput();
            central.addNewHearingAid(uniqueID, type);
            done = true;
        }
    }

    /**
     * This method prints the info of all hearing aids in the central
     */
    public void printAll(){
        System.out.println(central.getCentralName());
        System.out.println("Registrerte hjelpemidler");
        central.listAll();
    }

    /**
     * This method lets the user register a new rental of a hearing aid
     */
    public void registerNewRental(){
        System.out.println("Vennligst skriv inn den unike ID'en: ");
        String uniqueID = input.getStringInputBetween();
        System.out.println("Vennligst skriv navnet på den nye eieren: ");
        String nameOfCurrentOwner = input.getStringInput();
        central.registerRental(uniqueID, nameOfCurrentOwner, true);
    }

    /**
     * This method lets the user remove the rental status of a hearing aid
     */
    public void removeRental(){
        System.out.println("Vennligst skriv inn den unike ID'en: ");
        String uniqueID = input.getStringInputBetween();
        central.endRental(uniqueID);
    }

    /**
     * This method initializes the UI by using a switch-case
     */
    public void init(){
        System.out.println("Velkommen til hjelpemiddelsentralen, " + central.getCentralName());
        System.out.println("Skriv, HJELP, for å se de tilgjengelige kommandoene: ");
        boolean done = false;
        while (!done){
            UserInput input = new UserInput();
            String command = input.getStringInput().toUpperCase();
            switch (command){
                case "HJELP":
                    printHelp();
                    break;
                case "LEGG TIL":
                    addNewHearingAid();
                    break;
                case "PRINT":
                    printAll();
                    break;
                case "REGISTRER NY":
                    registerNewRental();
                    break;
                case "FJERN UTLEIE":
                    removeRental();
                    break;
                case "AVSLUTT":
                    done = true;
                    break;
                default:
                    System.out.println("Ukjent kommando. Se HJELP for kjente kommandoer:");
                    break;
            }
        }
    }

    /**
     * This is the main method that runs the entire program
     * @param args Takes arguments of type String array
     */
    public static void main(String[] args) {
        HearingAidCentralUI hearingAidCentralUI = new HearingAidCentralUI();
        hearingAidCentralUI.addTests();
        hearingAidCentralUI.init();

    }

}

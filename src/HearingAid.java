/**
 * The class generates objects that are meant to simulate different instances of a hearing aid
 * @author 10161
 * @version 12.12.2019
 */
public class HearingAid {
    /**
     * This field holds the unique ID of the hearing aid, as a String
     */
    private String uniqueID;
    /**
     * This field holds the type of hearing aid, as a String
     */
    private String type;
    /**
     * This field holds the rental status of the hearing aid, as a boolean value (true/false)
     */
    private boolean rentalStatus;
    /**
     * This field holds the name of the current owner of the hearing aid, as a String
     */
    private String nameOfCurrentOwner;

    /**
     * This constructor creates instances of the class hearing aid
     * @param uniqueID This parameter inserts the unique ID, as a String
     * @param type This parameter inserts the type, as a String
     * @param rentalStatus This parameter inserts the rental status, as a boolean
     * @param nameOfCurrentOwner This parameter inserts the name of the current owner, as a String
     */
    public HearingAid(String uniqueID, String type, boolean rentalStatus, String nameOfCurrentOwner){
        this.uniqueID = uniqueID;
        this.type = type;
        this.rentalStatus = rentalStatus;
        this.nameOfCurrentOwner = nameOfCurrentOwner;
    }

    /**
     * This method accesses the unique ID field of an instance
     * @return Returns the unique ID value as a String
     */
    public String getUniqueID() {
        return uniqueID;
    }

    /**
     * This method accesses the type field of an instance
     * @return Returns the type value as a String
     */
    public String getType() {
        return type;
    }

    /**
     * This method accesses the rental status field of an instance
     * @return Returns the rental status value as a boolean
     */
    public boolean isRentalStatus() {
        return rentalStatus;
    }

    /**
     * This method accesses the name of the current owner field of an instance
     * @return Returns the name of the current owner value as a String
     */
    public String getNameOfCurrentOwnerOwner() {
        return nameOfCurrentOwner;
    }

    /**
     * Mutates the rental status field of an instance
     * @param rentalStatus Takes a boolean value and inserts that into the field of an instance
     */
    public void setRentalStatus(boolean rentalStatus) {
        this.rentalStatus = rentalStatus;
    }

    /**
     * Mutates the name of current owner field of an instance
     * @param nameOfCurrentOwner Takes a String value and inserts that into the field of an instance
     */
    public void setNameOfOwner(String nameOfCurrentOwner) {
        this.nameOfCurrentOwner = nameOfCurrentOwner;
    }

    /**
     * Checks if two objects are similar by comparing their unique IDs
     * @param hearingAid1 Takes a HearingAid object
     * @param hearingAid2 Takes a HearingAid object
     * @return Returns the boolean value, true, if the objects have similar IDs, and false if they do not
     */
    public boolean compareHearingAid(HearingAid hearingAid1, HearingAid hearingAid2){
        boolean similar;
        similar = hearingAid1.getUniqueID().equals(hearingAid2.getUniqueID());
        return similar;
    }

    /**
     * Prints the info of a HearingAid object based on if it is rented or not
     * If it is rented it displays the first message
     * If it is not, it displays the second message
     */
    public void printInfo(){
        if (rentalStatus){
            System.out.println(uniqueID + " " + type + " utleid til " + nameOfCurrentOwner);
        } else {
            System.out.println(uniqueID + " " + type + " ledig");
        }
    }

}

import java.util.Scanner;

public class UserInput {
    private Scanner scanner;

    public UserInput(){
        scanner = new Scanner(System.in);
    }

    public String getStringInput(){
        String input = "";
        boolean valid = false;
        while (!valid){
            Scanner scanner = new Scanner(System.in);
            input = scanner.nextLine();
            if (input.equals("")){
                System.out.println("Input can not be empty. Please try again:");
            } else {
                valid = true;
            }
        }
        return input;
    }

    public String getStringInputBetween(){
        String input = "";
        boolean valid = false;
        while (!valid){
            Scanner scanner = new Scanner(System.in);
            input = scanner.nextLine();
            if (input.equals("")){
                System.out.println("Input can not be empty. Please try again:");
            } else if (Integer.parseInt(input) < 1001 || Integer.parseInt(input) > 9999){
                System.out.println("Den unike ID'en må være mellom 1001 og 9999");
            } else {
                valid = true;
            }
        }
        return input;
    }


}
